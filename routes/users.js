var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});


router.get("/list", async (req, res) => {
  console.log('First users API Route');
  try {
    const data = await connection.promise().query(`SELECT *  from users;`);
    res.status(200).json({
      users: data[0],
    });
  } catch (err) {
    res.status(500).json({
      message: err,
    });
  }
});

// app.get("/users", async (req, res) => {
//   try {
//     const data = await connection.promise().query(`SELECT *  from users;`);
//     res.status(200).json({
//       users: data[0],
//     });
//   } catch (err) {
//     res.status(500).json({
//       message: err,
//     });
//   }
// });

// app.get("/user/:id", async (req, res) => {
//   try {
//     const { id } = req.params;
//     const data = await connection
//       .promise()
//       .query(`SELECT *  from users where id = ?`, [id]);
//     res.status(200).json({
//       user: data[0][0],
//     });
//   } catch (err) {
//     res.status(500).json({
//       message: err,
//     });
//   }
// });

// app.patch("/user/:id", async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { name, address, country } = req.body;
//     const update = await connection
//       .promise()
//       .query(
//         `UPDATE users set name = ?, address = ?, country = ? where id = ?`,
//         [name, address, country, id]
//       );
//     res.status(200).json({
//       message: "updated",
//     });
//   } catch (err) {
//     res.status(500).json({
//       message: err,
//     });
//   }
// });

// app.delete("/user/:id", async (req, res) => {
//   try {
//     const { id } = req.params;
//     const update = await connection
//       .promise()
//       .query(
//         `DELETE FROM  users where id = ?`,
//         [id]
//       );
//     res.status(200).json({
//       message: "deleted",
//     });
//   } catch (err) {
//     res.status(500).json({
//       message: err,
//     });
//   }
// });





module.exports = router;
