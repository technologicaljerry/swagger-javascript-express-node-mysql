import mysql from "mysql2";
// const mysql = require('mysql2');

const connection = mysql.createPool({
    host: "localhost",
    user: "root",
    password: "MSroot@office",
    database: "swagger-javascript-express-node-mysql",
});

connection.getConnection((err, conn) => {
    if (err) {
        console.error('Error connecting to database:', err);
    } else {
        console.log('Database connected successfully!');
        conn.release();
    }
});

module.exports = connection;
